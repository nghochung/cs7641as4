# CS7641 Assignment 4 - Reinforcement Learning
# Student: Nguyen Ho Chung - GTID: cnguyen99

Link:
https://github.gatech.edu/cnguyen99/cs7641as4

Problem 1:
Run the experiment:
mvn exec:java -q -Dexec.mainClass="edu.brown.cs.burlap.tutorials.GridWorldSmall" -Dexec.args="--algorithm VI --iterations 300 --visualize true"


Problem 2:
Run the experiment:
mvn exec:java -q -Dexec.mainClass="edu.brown.cs.burlap.tutorials.GridWorldBig" -Dexec.args="--algorithm QL --iterations 120000 --gamma 0.99999 --maxDelta 0.001 --visualize true --greedyEpsilon 0.6 --transSuccessProb 0.8 --output qlresultbig1.txt --learningRate 0.5"

Arguments:
  -h, --help             show this help message and exit
  --algorithm A          Algorithm (VI/PI/QL)
  --gamma G              Discount rate
  --transSuccessProb T   Transition success prob
  --maxDelta D           Max delta
  --iterations I         Number of iterations
  --learningRate L       Learning rate
  --greedyEpsilon G      Greedy epsilon: prob for exploring non-optimal actions
  --visualize V          Visualize
  --output O             Output result