#!/bin/bash
# Basic range in for loop
for maxDelta in 0.0001 0.0005 0.001 0.005 0.01
do
    echo $maxDelta
    mvn exec:java -q -Dexec.mainClass="edu.brown.cs.burlap.tutorials.GridWorldSmall" -Dexec.args="--algorithm PI --iterations 600 --maxDelta $maxDelta --output resultSmall.txt"
done
echo All done