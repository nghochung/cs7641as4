package edu.brown.cs.burlap.tutorials;

import burlap.behavior.policy.EpsilonGreedy;
import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.policy.PolicyUtils;
import burlap.behavior.singleagent.Episode;
import burlap.behavior.singleagent.auxiliary.EpisodeSequenceVisualizer;
import burlap.behavior.singleagent.auxiliary.StateReachability;
import burlap.behavior.singleagent.auxiliary.performance.LearningAlgorithmExperimenter;
import burlap.behavior.singleagent.auxiliary.performance.PerformanceMetric;
import burlap.behavior.singleagent.auxiliary.performance.TrialMode;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.ValueFunctionVisualizerGUI;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.ArrowActionGlyph;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.LandmarkColorBlendInterpolation;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.PolicyGlyphPainter2D;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.StateValuePainter2D;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.learning.LearningAgentFactory;
import burlap.behavior.singleagent.learning.tdmethods.QLearning;
import burlap.behavior.singleagent.learning.tdmethods.SarsaLam;
import burlap.behavior.singleagent.planning.Planner;
import burlap.behavior.singleagent.planning.deterministic.DeterministicPlanner;
import burlap.behavior.singleagent.planning.deterministic.informed.Heuristic;
import burlap.behavior.singleagent.planning.deterministic.informed.astar.AStar;
import burlap.behavior.singleagent.planning.deterministic.uninformed.bfs.BFS;
import burlap.behavior.singleagent.planning.deterministic.uninformed.dfs.DFS;
import burlap.behavior.singleagent.planning.stochastic.policyiteration.PolicyIteration;
import burlap.behavior.singleagent.planning.stochastic.valueiteration.ValueIteration;
import burlap.behavior.valuefunction.QProvider;
import burlap.behavior.valuefunction.ValueFunction;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.singleagent.gridworld.GridWorldTerminalFunction;
import burlap.domain.singleagent.gridworld.GridWorldVisualizer;
import burlap.domain.singleagent.gridworld.state.GridAgent;
import burlap.domain.singleagent.gridworld.state.GridLocation;
import burlap.domain.singleagent.gridworld.state.GridWorldState;
import burlap.mdp.auxiliary.common.SinglePFTF;
import burlap.mdp.auxiliary.stateconditiontest.StateConditionTest;
import burlap.mdp.auxiliary.stateconditiontest.TFGoalCondition;
import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.oo.propositional.PropositionalFunction;
import burlap.mdp.core.state.State;
import burlap.mdp.core.state.vardomain.VariableDomain;
import burlap.mdp.singleagent.common.GoalBasedRF;
import burlap.mdp.singleagent.common.VisualActionObserver;
import burlap.mdp.singleagent.environment.Environment;
import burlap.mdp.singleagent.environment.SimulatedEnvironment;
import burlap.mdp.singleagent.model.FactoredModel;
import burlap.mdp.singleagent.model.RewardFunction;
import burlap.mdp.singleagent.oo.OOSADomain;
import burlap.statehashing.HashableState;
import burlap.statehashing.HashableStateFactory;
import burlap.statehashing.simple.SimpleHashableStateFactory;
import burlap.visualizer.Visualizer;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

/**
 * @author cnguyen99
 */

public class GridWorldSmall {
	public enum AlgorithmEnums {
		VI, PI, QL
	};

	AlgorithmEnums algo;
	GridWorldDomain gwdg;
	OOSADomain domain;
	GridWorldRF rf;
	TerminalFunction tf;
	StateConditionTest goalCondition;
	State initialState;
	HashableStateFactory hashingFactory;
	SimulatedEnvironment env;
	PrintStream fileout = null;

	// measure vars
	long elapsedNSEC = 0;
	long planningTimeNSEC = 0;
	long rolloutTimeNSEC = 0;
	int numSteps = 0;

	// settings
	double gamma = 0.99;
	String outputPath = "output/"; //directory to record results
	int maxIter = 100;
	boolean visualize = true;
	double totalReturn = 0.0;
	int mapSize = 5;
	String resultFile = "result.txt";
	double maxDelta = 0.001;
	double learningRate = 0.5;
	double greedyEpsilon = 0.1;
	double transSuccessProg = 0.8;

	public GridWorldSmall() {
	}

	protected void init() {
		/**
		 * Empty world with a danger zone D in the middle. There are 2 ways to get from initia state I to goal G,
		 * go straight through the narrow safe passage, or go around the danger zone.
		 * 
		 * As the transition gets more uncertain, the agent will less likely choose the former route because any
		 * wrong move will incur penalty.
		 * 
		 * Y
		 * |
		 * +---+---+---+---+---+---+---+
		 * | G |   |   |   |   |   |   |
		 * +---+---+---+---+---+---+---+
		 * |   |   |   |   |   |   |   |
		 * +---+---+---+---+---+---+---+
		 * |   | D | D | D |   |   |   |
		 * +---+---+---+---+---+---+---+
		 * |   | D | D | D |   |   |   |
		 * +---+---+---+---+---+---+---+
		 * |   | D | D | D |   |   |   |
		 * +---+---+---+---+---+---+---+
		 * |   |   |   |   |   |   |   |
		 * +---+---+---+---+---+---+---+
		 * | I |   |   |   |   |   |   |
		 * +---+---+---+---+---+---+---+-- X
		 */
	
		mapSize = 7;
		gwdg = new GridWorldDomain(mapSize, mapSize);
		gwdg.makeEmptyMap();

		gwdg.setProbSucceedTransitionDynamics(transSuccessProg); //stochastic transitions with < 1.0 success rate

		//ends when the agent reaches a location
		tf = new GridWorldTerminalFunction(0, 6);

		//reward function definition
		rf = new GridWorldRF(new TFGoalCondition(tf), 5.0 /* goalReward */, -0.01 /* defaultReward */);
		rf.addDangerRect(-10.0 /* dangerReward */, 1, 2, 3, 3);

		gwdg.setTf(tf);
		gwdg.setRf(rf);

		goalCondition = new TFGoalCondition(tf);
		domain = gwdg.generateDomain();
	
		initialState = new GridWorldState(new GridAgent(0, 0), new GridLocation(0, 0, "loc0"));
		hashingFactory = new SimpleHashableStateFactory();
	
		env = new SimulatedEnvironment(domain, initialState);
	}

	protected void runVI() {
		long startTime = System.nanoTime();

		Planner planner = new CustomValueIteration(domain, gamma, hashingFactory, maxDelta, maxIter);
		long planningEndTime = System.nanoTime();

		Policy p = planner.planFromState(initialState);
		Episode e = PolicyUtils.rollout(p, initialState, domain.getModel());
		e.write(outputPath + "vi");
	
		// get difference of two nanoTime values
		long endTime = System.nanoTime();
		elapsedNSEC = endTime - startTime;
		planningTimeNSEC = planningEndTime - startTime;
		rolloutTimeNSEC = endTime - planningEndTime;

		numSteps = e.numTimeSteps();

		totalReturn = e.discountedReturn(gamma);

		if (visualize) {
			//create visualizer and explorer
			Visualizer v = GridWorldVisualizer.getVisualizer(gwdg.getMap());
			new EpisodeSequenceVisualizer(v, domain, Collections.singletonList(e));	

			simpleValueFunctionVis((ValueFunction)planner, p, String.format("%s: Value Function", algo.name()));
			simplePathVis(p, e, String.format("%s: Visited states", algo.name()));
		}
	}

	protected void runPI() {
		long startTime = System.nanoTime();

		Planner planner = new CustomPolicyIteration(domain, gamma, hashingFactory, maxDelta, maxIter, maxIter);
		Policy p = planner.planFromState(initialState);
		long planningEndTime = System.nanoTime();

		Episode e = PolicyUtils.rollout(p, initialState, domain.getModel());
		e.write(outputPath + "pi");
	
		// get difference of two nanoTime values
		long endTime = System.nanoTime();
		elapsedNSEC = endTime - startTime;
		planningTimeNSEC = planningEndTime - startTime;
		rolloutTimeNSEC = endTime - planningEndTime;

		numSteps = e.numTimeSteps();

		totalReturn = e.discountedReturn(gamma);
	
		if (visualize) {
			//create visualizer and explorer
			Visualizer v = GridWorldVisualizer.getVisualizer(gwdg.getMap());
			new EpisodeSequenceVisualizer(v, domain, Collections.singletonList(e));	
			
			simpleValueFunctionVis((ValueFunction)planner, p, String.format("%s: Value Function", algo.name()));
			simplePathVis(p, e, String.format("%s: Visited states", algo.name()));
		}
	}

	protected void dualPrint(final String line) {
		System.out.print(line);
		fileout.print(line);
	}
		
	protected void runQL() {
		long startTime = System.nanoTime();

		// Q Learning
		
		CustomQLearning agent = new CustomQLearning(domain, gamma, hashingFactory, 0., learningRate);
		EpsilonGreedy learningPolicy = new EpsilonGreedy(agent, greedyEpsilon);
		agent.setMaxQChangeForPlanningTerminaiton(this.maxDelta);
		agent.setLearningPolicy(learningPolicy);
		double bestReturn = -Double.MAX_VALUE;
		Episode bestEpisode = null;
		int bestEpisodeIdx = -1;

		final double exploreEndEpsilon = 0.0001;
		final double exploreIterPerc = 0.9;

		final double DECAY_FACTOR = Math.exp(Math.log(exploreEndEpsilon/greedyEpsilon)/(exploreIterPerc*maxIter));
		double epsilon = greedyEpsilon;

		Rolling rollingReturn = new Rolling(1000);

		int qLearningMaxSteps = 2 * mapSize * mapSize;
		// int qLearningMaxSteps = -1;
		
		String header = String.format("%15s %15s %15s %15s %15s %15s\n", "Iter", "NbSteps", "ExploreProb", "Return", "RollingRet", "MaxQChange");
		System.out.print(header);

		try {
			fileout = new PrintStream(new FileOutputStream(this.resultFile, false /* append */));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		fileout.print(header);

		//run learning for a number of episodes
		for(int i = 0; i < maxIter; i++){
			Episode e = agent.runLearningEpisode(env, qLearningMaxSteps);
	
			double ret = e.discountedReturn(gamma);
			rollingReturn.add(ret);

			String line = String.format("%15d %15d %15.5f %15.5f %15.5f %15.5f\n", i, e.maxTimeStep(), epsilon, ret, rollingReturn.getAverage(), agent.maxQChangeInLastEpisode());

			if (ret > bestReturn) {
				bestEpisode = e;
				bestEpisodeIdx = i;
				bestReturn = ret;
				System.out.println(line);
			} else if (i % 100 == 0) {
				System.out.println(line);
			}

			if (i % 100 == 0) {
				// e.write(outputPath + "ql_" + i);
				fileout.print(line);
			}

			//reset environment for next learning episode
			env.resetEnvironment();
			epsilon *= DECAY_FACTOR;
			learningPolicy.setEpsilon(epsilon);
		}
		elapsedNSEC = System.nanoTime() - startTime;

		String line = "Best episode =" + bestEpisodeIdx + ", maxTimeStep = " + bestEpisode.maxTimeStep() + ", return = " + bestReturn + "\n";
		dualPrint(line);
		line = "Reward sequece: " + bestEpisode.rewardSequence.toString() + "\n";
		dualPrint(line);
		
		if (visualize) {
			simpleValueFunctionVis(agent, learningPolicy, String.format("%s: Value Function", algo.name()));
			// simplePathVis(learningPolicy, bestEpisode, String.format("%s: Visited states", algo.name()));
		}

		// Rollout the policy and measure reward
		int nbRollout = 100;
		List<Episode> rolloutEpisodes = new ArrayList<>(nbRollout);
		double rolloutTotalReturn = 0.0;
		line = String.format("%10s %10s %10s %10s\n", "RolloutEp", "NbSteps", "Terminated", "Return");
		dualPrint(line);
		final int maxRolloutSteps = 400 * mapSize * mapSize;

		for (int i = 0; i < nbRollout; ++i) {
			Environment env = new SimulatedEnvironment(domain.getModel(), initialState);
			Episode e = PolicyUtils.rollout(learningPolicy, env, maxRolloutSteps);
			e.write(outputPath + "qlro" + i);
			boolean terminated = env.isInTerminalState();
			double r = e.discountedReturn(this.gamma);

			if (terminated) {
				rolloutEpisodes.add(e);
				rolloutTotalReturn += r;
			}
			line = String.format("%10d %10d %10s %10.5f\n", i, e.maxTimeStep(), terminated ? "Y" : "N", r);
			dualPrint(line);
		}

		// sort the rolled-out episodes by reward
		final double gamma = this.gamma;
		rolloutEpisodes.sort(new Comparator<Episode>() {
			@Override public int compare(Episode a, Episode b) {
				final double ar = a.discountedReturn(gamma);
				final double br = b.discountedReturn(gamma);
				return (br > ar ? 1 : (br < ar ? -1 : 0));
			}
		});

		if (rolloutEpisodes.size() > 0) {
			// log return and nbsteps of median episode
			totalReturn = rolloutEpisodes.get(rolloutEpisodes.size() / 2).discountedReturn(this.gamma);
			numSteps = rolloutEpisodes.get(rolloutEpisodes.size() / 2).numTimeSteps();	
		}

		line = String.format("Rollout complete = %d/%d\n", rolloutEpisodes.size(), nbRollout);
		dualPrint(line);
		
		line = String.format("Rollout avg return = %15.5f\n", rolloutTotalReturn/nbRollout);
		dualPrint(line);


		if (visualize) {
			Visualizer v = GridWorldVisualizer.getVisualizer(gwdg.getMap());
			new EpisodeSequenceVisualizer(v, domain, rolloutEpisodes);

			if (rolloutEpisodes.size() >= 5) {
				// show paths of 0-percentile, 25-pencentile, etc (sorted by return)
				final int di = rolloutEpisodes.size() >= 4 ? rolloutEpisodes.size()/4 : 1;
				int idx = 0;
				for (int i = 0; i <= 4; i++) {
					Episode e = rolloutEpisodes.get(idx);
					simplePathVis(learningPolicy, e, String.format("QL: Rollout %d-percentile NbSteps %d, return %10.5f", i*25, e.maxTimeStep(), e.discountedReturn(this.gamma)));
					idx += di;
					if (idx >= rolloutEpisodes.size())
						idx = rolloutEpisodes.size() - 1;
				}
			}
		}

		fileout.close();
	}

	protected void simpleValueFunctionVis(ValueFunction valueFunction, Policy p, String title) {
		List<State> allStates = StateReachability.getReachableStates(initialState, 
				domain, hashingFactory);
		ValueFunctionVisualizerGUI gui = GridWorldDomain.getGridWorldValueFunctionVisualization(
			allStates, mapSize, mapSize, valueFunction, p);
		gui.setTitle(title);
		gui.setPreferredSize(new Dimension(480, 480));
    	gui.pack(); 
		gui.initGUI();
	}

	protected Map<HashableState, Integer> getVisitMap(Episode episode) {
		Map<HashableState, Integer> visitMap = new HashMap<>();

		for (State s : episode.stateSequence) {
			HashableState hs = hashingFactory.hashState(s);
			Integer nbVisits = visitMap.getOrDefault(hs, 0);
			visitMap.put(hs, nbVisits + 1);
		}

		return visitMap;
	}

	protected void simplePathVis(final Policy p, final Episode episode, String title) {
		final Map<HashableState, Integer> visitMap = getVisitMap(episode);

		final List<State> allStates = StateReachability.getReachableStates(initialState, 
				domain, hashingFactory);
		ValueFunction valueFunction = new ValueFunction(){
		
			@Override
			public double value(State s) {
				HashableState hs = hashingFactory.hashState(s);
				int nbVisits =  visitMap.getOrDefault(hs, 0);
				return (double)nbVisits;
			}
		};
		ValueFunctionVisualizerGUI gui = GridWorldDomain.getGridWorldValueFunctionVisualization(
			allStates, mapSize, mapSize, valueFunction, p);


		LandmarkColorBlendInterpolation rb = new LandmarkColorBlendInterpolation() {
			@Override public Color color(double v) {
				return v == 0.0 ? Color.DARK_GRAY : super.color(v);
			}
		};
		rb.addNextLandMark(0., Color.GRAY);
		rb.addNextLandMark(1., Color.WHITE);
		// rb.addNextLandMark(0., Color.RED);
		// rb.addNextLandMark(1., Color.BLUE);
	
		((StateValuePainter2D)gui.getSvp()).setColorBlend(rb);
		gui.setTitle(title);
		gui.setPreferredSize(new Dimension(480, 480));
    	gui.pack();

		gui.initGUI();
	}

	protected void printResult() {
		String header = String.format("%10s %10s %10s %10s %10s %15s %15s %15s\n", "Algorithm", "MaxIter", "MaxDelta", "Return", "NumSteps", "TimeMs", "PlanningTime", "RolloutTime");
		String line = String.format("%10s %10d %10.5f %10.2f %10d %15d %15d %15d\n", this.algo.name(), this.maxIter, this.maxDelta, this.totalReturn, this.numSteps, this.elapsedNSEC/1000000, this.planningTimeNSEC/1000000, this.rolloutTimeNSEC/1000000);

		System.out.print(header);
		System.out.print(line);

		boolean isExistingResultFile = new File(this.resultFile).isFile();

		try {
			PrintStream fileout = new PrintStream(new FileOutputStream(this.resultFile, true /*append*/));
			if (!isExistingResultFile) {
				fileout.print(header);
			}

			fileout.print(line);
			fileout.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	protected void run() {
		switch (algo) {
			case VI: runVI(); break;
			case PI: runPI(); break;
			case QL: runQL(); break;
		}
	}

	protected static ArgumentParser createArgumentParser(String name) {
		ArgumentParser parser = ArgumentParsers.newFor(name).build().description("Grid world");

		parser.addArgument("--algorithm")
			.metavar("A")
			.type(AlgorithmEnums.class)
			.setDefault(AlgorithmEnums.VI)
			.help("Algorithm (VI/PI/QL)");
		
		parser.addArgument("--gamma")
			.metavar("G")
			.type(Double.class)
			.setDefault(0.99)
			.help("Discount rate");

		parser.addArgument("--transSuccessProb")
			.metavar("T")
			.type(Double.class)
			.setDefault(0.8)
			.help("Transition success prob");

		parser.addArgument("--maxDelta")
			.metavar("D")
			.type(Double.class)
			.setDefault(0.001)
			.help("Max delta");

		parser.addArgument("--iterations")
			.metavar("I")
			.type(Integer.class)
			.setDefault(100)
			.help("Number of iterations");
		
		parser.addArgument("--learningRate")
			.metavar("L")
			.type(Double.class)
			.setDefault(0.5)
			.help("Learning rate");

		parser.addArgument("--greedyEpsilon")
			.metavar("G")
			.type(Double.class)
			.setDefault(0.1)
			.help("Greedy epsilon: prob for exploring non-optimal actions");

		parser.addArgument("--visualize")
			.metavar("V")
			.type(Boolean.class)
			.setDefault(Boolean.FALSE)
			.help("Visualize");

		parser.addArgument("--output")
			.metavar("O")
			.type(String.class)
			.setDefault("result.txt")
			.help("Output result");

		return parser;
	}

	protected void initParams(Namespace res) {
		transSuccessProg = res.get("transSuccessProb");
		maxIter = res.get("iterations");
		maxDelta = res.get("maxDelta");
		greedyEpsilon = res.get("greedyEpsilon");
		learningRate = res.get("learningRate");
		visualize = res.get("visualize");
		algo = res.get("algorithm");
		resultFile = res.get("output");
	}

	public static void main(String[] args) {
		ArgumentParser parser = createArgumentParser("GridWorldSmall");

		Namespace res = null;

		try {
			res = parser.parseArgs(args);
		} catch (ArgumentParserException e) {
			parser.handleError(e);
		}
		
		GridWorldSmall world = new GridWorldSmall();
		world.initParams(res);
		world.init();
		world.run();
		world.printResult();
	}
}
