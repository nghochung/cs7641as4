package edu.brown.cs.burlap.tutorials;

import burlap.mdp.auxiliary.stateconditiontest.StateConditionTest;
import burlap.mdp.auxiliary.stateconditiontest.TFGoalCondition;
import burlap.mdp.core.action.Action;
import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.model.RewardFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import burlap.domain.singleagent.gridworld.GridWorldTerminalFunction.IntPair;
import burlap.domain.singleagent.gridworld.state.GridWorldState;

/**
 * A reward function implementation designed around goal conditions that are specified by a {@link burlap.mdp.auxiliary.stateconditiontest.StateConditionTest} object
 * or a {@link burlap.mdp.core.TerminalFunction}.
 * When the agent transition to a state marked as a goal state, it returns a goal reward. Otherwise a default reward is returned.
 * @author James MacGlashan
 *
 */
public class GridWorldRF implements RewardFunction {

	protected StateConditionTest gc;
	protected double goalReward = 1.0;
    protected double defaultReward = 0.0;
    protected List<Rect> dangerZones;

	/**
	 * Initializes with transitions to goal states returning the given reward and all others returning 0.
	 * @param gc {@link burlap.mdp.auxiliary.stateconditiontest.StateConditionTest} object that specifies goal states.
	 * @param goalReward the reward returned for transitions to goal states.
	 * @param defaultReward the default reward returned for all non-goal state transitions.
	 */
	public GridWorldRF(StateConditionTest gc, double goalReward, double defaultReward, double dangerReward, int dangerX, int dangerY, int dangerW, int dangerH) {
		this.gc = gc;
		this.goalReward = goalReward;
        this.defaultReward = defaultReward;
		this.dangerZones = new ArrayList<>();
		this.dangerZones.add(new Rect(dangerReward, dangerX, dangerY, dangerW, dangerH));
	}

	/**
	 * Initializes with transitions to goal states returning the given reward and all others returning 0.
	 * @param gc {@link burlap.mdp.auxiliary.stateconditiontest.StateConditionTest} object that specifies goal states.
	 * @param goalReward the reward returned for transitions to goal states.
	 * @param defaultReward the default reward returned for all non-goal state transitions.
	 */
	public GridWorldRF(StateConditionTest gc, double goalReward, double defaultReward) {
		this.gc = gc;
		this.goalReward = goalReward;
        this.defaultReward = defaultReward;
        this.dangerZones = new ArrayList<>();
	}

	public void addDangerRect(double reward, int dangerX, int dangerY, int dangerW, int dangerH) {
		this.dangerZones.add(new Rect(reward, dangerX, dangerY, dangerW, dangerH));
	}

	/**
	 * Returns the goal condition for this reward function.
	 * @return the goal condition for this reward function.
	 */
	public StateConditionTest getGoalCondition(){
		return this.gc;
	}


	public double getGoalReward() {
		return goalReward;
	}

	public void setGoalReward(double goalReward) {
		this.goalReward = goalReward;
	}

	public double getDefaultReward() {
		return defaultReward;
	}

	public void setDefaultReward(double defaultReward) {
		this.defaultReward = defaultReward;
	}

	@Override
	public double reward(State s, Action a, State sprime) {
		
		if(gc.satisfies(sprime)){
			return goalReward;
        }
        
        int x = ((GridWorldState)s).agent.x;
		int y = ((GridWorldState)s).agent.y;

		for (Rect rect : dangerZones) {
			if (rect.contains(x, y)) {
				return rect.reward();
			}
		}
		
		return defaultReward;
    }
    
    public class Rect {
		protected double reward;
        protected int x;
        protected int y;
        protected int w;
        protected int h;

        public Rect(double reward, int x, int y, int w, int h){
			this.reward = reward;
			this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }
        
        public boolean contains(int x, int y) {
            return this.x <= x && this.x + this.w > x &&
                this.y <= y && this.y + this.h > y;
        }
		
		@Override
		public int hashCode(){
			return this.x + 31*this.y;
		}
		
		@Override
		public boolean equals(Object other){
			if(!(other instanceof Rect)){
				return false;
			}
            
            Rect o = (Rect) other;
			return this.x == o.x && this.y == o.y && this.w == o.w && this.h == o.h;
		}

		public double reward() { return this.reward;}
	}


}
