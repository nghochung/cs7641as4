package edu.brown.cs.burlap.tutorials;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.policy.PolicyUtils;
import burlap.behavior.singleagent.Episode;
import burlap.behavior.singleagent.auxiliary.EpisodeSequenceVisualizer;
import burlap.behavior.singleagent.auxiliary.StateReachability;
import burlap.behavior.singleagent.auxiliary.performance.LearningAlgorithmExperimenter;
import burlap.behavior.singleagent.auxiliary.performance.PerformanceMetric;
import burlap.behavior.singleagent.auxiliary.performance.TrialMode;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.ValueFunctionVisualizerGUI;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.ArrowActionGlyph;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.LandmarkColorBlendInterpolation;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.PolicyGlyphPainter2D;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.common.StateValuePainter2D;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.learning.LearningAgentFactory;
import burlap.behavior.singleagent.learning.tdmethods.QLearning;
import burlap.behavior.singleagent.learning.tdmethods.SarsaLam;
import burlap.behavior.singleagent.planning.Planner;
import burlap.behavior.singleagent.planning.deterministic.DeterministicPlanner;
import burlap.behavior.singleagent.planning.deterministic.informed.Heuristic;
import burlap.behavior.singleagent.planning.deterministic.informed.astar.AStar;
import burlap.behavior.singleagent.planning.deterministic.uninformed.bfs.BFS;
import burlap.behavior.singleagent.planning.deterministic.uninformed.dfs.DFS;
import burlap.behavior.singleagent.planning.stochastic.policyiteration.PolicyIteration;
import burlap.behavior.singleagent.planning.stochastic.valueiteration.ValueIteration;
import burlap.behavior.valuefunction.QProvider;
import burlap.behavior.valuefunction.ValueFunction;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.singleagent.gridworld.GridWorldTerminalFunction;
import burlap.domain.singleagent.gridworld.GridWorldVisualizer;
import burlap.domain.singleagent.gridworld.state.GridAgent;
import burlap.domain.singleagent.gridworld.state.GridLocation;
import burlap.domain.singleagent.gridworld.state.GridWorldState;
import burlap.mdp.auxiliary.common.SinglePFTF;
import burlap.mdp.auxiliary.stateconditiontest.StateConditionTest;
import burlap.mdp.auxiliary.stateconditiontest.TFGoalCondition;
import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.oo.propositional.PropositionalFunction;
import burlap.mdp.core.state.State;
import burlap.mdp.core.state.vardomain.VariableDomain;
import burlap.mdp.singleagent.common.GoalBasedRF;
import burlap.mdp.singleagent.common.VisualActionObserver;
import burlap.mdp.singleagent.environment.SimulatedEnvironment;
import burlap.mdp.singleagent.model.FactoredModel;
import burlap.mdp.singleagent.model.RewardFunction;
import burlap.mdp.singleagent.oo.OOSADomain;
import burlap.statehashing.HashableStateFactory;
import burlap.statehashing.simple.SimpleHashableStateFactory;
import burlap.visualizer.Visualizer;

import java.awt.*;
import java.util.Collections;
import java.util.List;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

/**
 * @author cnguyen99
 */
public class GridWorldBig extends GridWorldSmall {

	public GridWorldBig() {
	}

    @Override
	protected void init() {
		/**
		 * Divide world into 4 quadrants, within each we have a smaller danger zone
		 * of different penalties. Walking on the edge, however, is small penalty -0.01
		 * 
		 * Y
		 * |
		 * E-------+-------+
		 * |       |       |
		 * |  -10  |  -5   |
		 * +---+---+---+---+
		 * |       |       |
		 * |  -10  |  -5   |
		 * I-------+-------+--->  X
		 * 
		 * 
		 */
	
        mapSize = 16;
		gwdg = new GridWorldDomain(mapSize, mapSize);
		gwdg.makeEmptyMap();

		gwdg.setProbSucceedTransitionDynamics(transSuccessProg); //stochastic transitions with < 1.0 success rate

		//ends when the agent reaches a location
		tf = new GridWorldTerminalFunction(0, mapSize - 1);

		//reward function definition
		rf = new GridWorldRF(new TFGoalCondition(tf), 5.0 /* goalReward */, -0.01 /* defaultReward */);
		int halfSize = mapSize / 2;
		int dangerSize = halfSize - 3;
        rf.addDangerRect(-10.0, 1, 1, dangerSize, dangerSize); // bottom left
        rf.addDangerRect(-10.0, 1, halfSize + 1, dangerSize, dangerSize); // top left
        rf.addDangerRect(-5.0, halfSize, 1, dangerSize, dangerSize); // bottom right
        rf.addDangerRect(-5.0, halfSize, halfSize + 1, dangerSize, dangerSize); /// top right

		gwdg.setTf(tf);
		gwdg.setRf(rf);

		goalCondition = new TFGoalCondition(tf);
		domain = gwdg.generateDomain();
	
		initialState = new GridWorldState(new GridAgent(0, 0), new GridLocation(0, 0, "loc0"));
		hashingFactory = new SimpleHashableStateFactory();
	
		env = new SimulatedEnvironment(domain, initialState);
	}

	public static void main(String[] args) {
		ArgumentParser parser = createArgumentParser("GridWorldBig");

		Namespace res = null;

		try {
			res = parser.parseArgs(args);
		} catch (ArgumentParserException e) {
			parser.handleError(e);
		}
		
		GridWorldBig world = new GridWorldBig();
		world.initParams(res);
		world.init();
		world.run();
		world.printResult();
    }

}
